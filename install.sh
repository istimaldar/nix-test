#!/usr/bin/env bash

set -euxo pipefail

cd "$(dirname "$0")"

read_password() {
    local prompt="$1"
    local password
    local password_repeat

    read -rsp "Enter ${prompt}:" password
    read -rsp "Confirm ${prompt}:" password_repeat

    if [[ ${#password} -lt 5 ]]; then echo "Password must be at lest 5 characters long!"; exit 1; fi
    if [[ "${password}" != "${password_repeat}" ]]; then echo "Password and confirmation did not match!"; exit 1; fi

    printf '%s' "${password}"
}

read_drive_name() {
  local drive_name

  read -rp "Enter the drive name to install linux on. Drive should be in format sd? (eg. sda):" drive_name

  if [[ ! -b "/dev/${drive_name}" ]]; then echo "Device /dev/${drive_name} does not exist."; exit 1; fi

  printf '%s' "/dev/${drive_name}"
}

read_swap_size() {
  local swap_size

  read -rp "Enter swap size in gigabytes (just a number):" swap_size

  if [[ ! "${swap_size}" =~ ^[0-9]+$ ]]; then echo "${swap_size} is not a number."; exit 1; fi

  printf '%s' "${swap_size}G"
}

if [[ "$(id -u)" -ne 0 ]]; then echo "You are not root. Please, run this script as root!"; exit 1; fi

drive_name="$(read_drive_name)"
swap_size="$(read_swap_size)"
encryption_key="$(read_password "drive encryption key")"
user_password="$(read_password "user password" | mkpasswd -m sha-512 -s)"

nix-env -iA nixos.git

parted -s "${drive_name}" -- mklabel gpt
parted -s "${drive_name}" -- mkpart ESP fat32 1MiB 512MiB
parted -s "${drive_name}" -- set 1 esp on
parted -s "${drive_name}" -- mkpart primary 512MiB 100%

printf '%s' "${encryption_key}" | cryptsetup luksFormat "${drive_name}2" -d-
printf '%s' "${encryption_key}" | cryptsetup luksOpen "${drive_name}2" nix-enc -d-

pvcreate /dev/mapper/nix-enc
vgcreate nix /dev/mapper/nix-enc
lvcreate -L "${swap_size}" -n swap nix
lvcreate -l '100%FREE' -n root nix

mkfs.fat "${drive_name}1"
mkfs.btrfs -L root /dev/nix/root
mkswap -L swap /dev/nix/swap

mount /dev/nix/root /mnt
mkdir /mnt/boot
mount "${drive_name}1" /mnt/boot
swapon /dev/nix/swap

nixos-generate-config --root /mnt

mv /mnt/etc/nixos/hardware-configuration.nix /tmp
rm -f /mnt/etc/nixos/configuration.nix

git clone https://gitlab.com/istimaldar/nix-test.git /mnt/etc/nixos

mv /tmp/hardware-configuration.nix /mnt/etc/nixos/

cat <<USERS > /mnt/etc/nixos/passwords.nix
{ config, ... } : 

{
  users.users = {
    istimaldar.hashedPassword = "${user_password}";
    root.hashedPassword = "${user_password}";
  };
  boot.initrd.luks.devices = {
    root = {
      device = "${drive_name}2";
    };
  };
}
USERS

nix-channel --add https://nixos.org/channels/nixos-unstable
nix-channel --add https://nixos.org/channels/nixpkgs-unstable
nixos-rebuild switch --upgrade
nixos-install

reboot
