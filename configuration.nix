{ config, pkgs, ... }:
let
  home-manager = builtins.fetchGit {
    url = "https://github.com/rycee/home-manager.git";
    ref = "release-21.05";
  };
  configDirectory = "/home/istimaldar/.config";
in
{
  imports =
    [
      ./hardware-configuration.nix
      ./passwords.nix
      (import "${home-manager}/nixos")
    ];

  # Supposedly better for the SSD.
  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      grub = {
        enable = true;
        version = 2;
        device = "nodev";
        useOSProber = true;
        efiSupport = true;
        gfxmodeEfi = "1920x800";
      };
      efi.canTouchEfiVariables = true;
    };
    initrd.luks.devices = {
      root = {
        name = "root";
        preLVM = true;
        allowDiscards = true;
      };
    };
  };

  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  networking.useDHCP = false;

  i18n = {
    defaultLocale = "en_US.UTF-8";
    supportedLocales = [ "en_US.UTF-8/UTF-8" "be_BY.UTF-8/UTF-8" "ru_RU.UTF-8/UTF-8" ];
  };

  nixpkgs.config.allowUnfree = true;

  users = {
    mutableUsers = false;
    users = {
      istimaldar = {
        isNormalUser = true;
        createHome = true;
        extraGroups = [ "wheel" "networkmanager" "docker" "audio" "pulse-access" ];
        shell = pkgs.zsh;
      };
    };
  };

  environment.systemPackages = (with pkgs; [
    git
    neovim
    i3-gaps
    i3status-rust
    picom
    alacritty
    zsh
    rofi
    dunst
    picom
    polybar
    networkmanager
    lightdm
    lightdm-enso-os-greeter
    xclip
    lm_sensors
    ethtool
    nixpkgs-fmt

    oh-my-zsh
    fzf
    zsh-autosuggestions
    zsh-syntax-highlighting
    zsh-powerlevel10k

    keepassxc
    firefox
    chromium

    dotnet-sdk
    dotnet-sdk_3
    dotnet-sdk_6
    jdk
    jdk11
    powershell
    nodejs-16_x
    python310
    kotlin
    go_1_17
    terraform
    terragrunt
    ansible
    awscli2

    vscode-with-extensions
    jetbrains.jdk
    jetbrains.idea-ultimate
    jetbrains.pycharm-professional
    jetbrains.rider
    jetbrains.clion
    jetbrains.goland
    jetbrains.webstorm
    jetbrains.phpstorm
    jetbrains.datagrip
    jetbrains.ruby-mine
    jetbrains.mps

    slack
    element-desktop
    skypeforlinux
    tdesktop

    qbittorrent

    podman
    buildah
    skopeo
    docker-compose_2
    dive
    kind
    kubectl
    kubernetes-helm
    helmfile
    helmsman
    k9s

    blender

    megacmd
  ])
  ++
  (with pkgs.nodePackages; [
    npm
  ]);

  fonts = {
    fonts = with pkgs; [
      nerdfonts
    ];

    fontconfig.defaultFonts = {
      monospace = [ "JetBrainsMono Nerd Font Mono" ];
      emoji = [ "JetBrainsMono Nerd Font Mono" "Noto Color Emoji" ];
    };
  };

  programs.zsh = {
    enable = true;
    ohMyZsh.enable = true;
  };

  services.xserver = {
    layout = "us,ru,by";
    xkbOptions = "grp:caps_toggle";
    videoDrivers = [ "nvidia" ];
    resolutions = [
      {
        x = 3440;
        y = 1440;
      }
    ];
    dpi = 110;
    enable = true;

    displayManager = {
      defaultSession = "none+i3";

      lightdm = {
        enable = true;

        greeter = {
          enable = true;
        };

        greeters.enso = {
          enable = true;
          blur = true;
        };
      };
    };

    windowManager.i3 = with pkgs; {
      enable = true;
      package = i3-gaps;
    };
  };

  networking.networkmanager.enable = true;

  home-manager = {
    useUserPackages = true;
    useGlobalPkgs = true;

    users.istimaldar = {
      xsession.windowManager.i3 = {
        enable = true;

        config = rec {
          modifier = "Mod4";
          terminal = "alacritty";
          window.border = 0;
          modes = {
            resize = {
              h = "resize shrink width 10 px or 10 ppt";
              j = "resize grow height 10 px or 10 ppt";
              k = "resize shrink height 10 px or 10 ppt";
              l = "resize grow width 10 px or 10 ppt";

              Left = "resize shrink width 10 px or 10 ppt";
              Down = "resize grow height 10 px or 10 ppt";
              Up = "resize shrink height 10 px or 10 ppt";
              Right = "resize grow width 10 px or 10 ppt";

              Escape = "mode default";
              Return = "mode default";
              "${modifier}+r" = "mode default";
            };
          };
          bars = [
            {
              statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ~/.config/i3status-rust/config-bottom.toml";
              fonts = [ "JetBrainsMono Nerd Font 12" ];
              position = "bottom";
              mode = "hide";
              colors = {
                separator = "#666666";
                background = "#3b4252";
                statusline = "#dddddd";

                focusedWorkspace = {
                  border = "#5e81ac";
                  background = "#5e81ac";
                  text = "#eceff4";
                };
                activeWorkspace = {
                  border = "#2e3440";
                  background = "#2e3440";
                  text = "#eceff4";
                };
                inactiveWorkspace = {
                  border = "#2e3440";
                  background = "#2e3440";
                  text = "#d8dee9";
                };
                urgentWorkspace = {
                  border = "#bf616a";
                  background = "#bf616a";
                  text = "#eceff4";
                };
              };
            }
          ];
          keybindings = {
            "${modifier}+Return" = "exec ${terminal}";
            "${modifier}+Shift+q" = "kill";
            "${modifier}+d" = "exec --no-startup-id dmenu_run";

            "${modifier}+h" = "focus left";
            "${modifier}+j" = "focus down";
            "${modifier}+k" = "focus up";
            "${modifier}+l" = "focus right";

            "${modifier}+Left" = "focus left";
            "${modifier}+Down" = "focus down";
            "${modifier}+Up" = "focus up";
            "${modifier}+Right" = "focus right";

            "${modifier}+Shift+h" = "move left";
            "${modifier}+Shift+j" = "move down";
            "${modifier}+Shift+k" = "move up";
            "${modifier}+Shift+l" = "move right";

            "${modifier}+Shift+Left" = "move left";
            "${modifier}+Shift+Down" = "move down";
            "${modifier}+Shift+Up" = "move up";
            "${modifier}+Shift+Right" = "move right";

            "${modifier}+g" = "split h";
            "${modifier}+v" = "split v";
            "${modifier}+f" = "fullscreen toggle";
            "${modifier}+s" = "layout stacking";
            "${modifier}+w" = "layout tabbed";
            "${modifier}+e" = "layout toggle split";
            "${modifier}+Shift+space" = "floating toggle";
            "${modifier}+space" = "focus mode_toggle";
            "${modifier}+a" = "focus parent";
            "${modifier}+c" = "focus child";
            "${modifier}+Shift+c" = "reload";
            "${modifier}+Shift+r" = "restart";
            "${modifier}+Shift+e" = "exec \"i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'\"";
            "${modifier}+r" = "mode \"resize\"";

            "${modifier}+1" = "workspace number 1";
            "${modifier}+2" = "workspace number 2";
            "${modifier}+3" = "workspace number 3";
            "${modifier}+4" = "workspace number 4";
            "${modifier}+5" = "workspace number 5";
            "${modifier}+6" = "workspace number 6";
            "${modifier}+7" = "workspace number 7";
            "${modifier}+8" = "workspace number 8";
            "${modifier}+9" = "workspace number 9";
            "${modifier}+0" = "workspace number 10";
            "${modifier}+minus" = "workspace number 11";
            "${modifier}+equal" = "workspace number 12";

            "${modifier}+Shift+1" = "move container to workspace number 1";
            "${modifier}+Shift+2" = "move container to workspace number 2";
            "${modifier}+Shift+3" = "move container to workspace number 3";
            "${modifier}+Shift+4" = "move container to workspace number 4";
            "${modifier}+Shift+5" = "move container to workspace number 5";
            "${modifier}+Shift+6" = "move container to workspace number 6";
            "${modifier}+Shift+7" = "move container to workspace number 7";
            "${modifier}+Shift+8" = "move container to workspace number 8";
            "${modifier}+Shift+9" = "move container to workspace number 9";
            "${modifier}+Shift+0" = "move container to workspace number 10";
            "${modifier}+Shift+minus" = "move container to workspace number 11";
            "${modifier}+Shift+equal" = "move container to workspace number 12";
          };
          gaps = {
            inner = 4;
            outer = -3;
            top = 0;
          };
          startup = [
          ];
        };
        extraConfig = ''
          for_window [class="^.*"] border pixel 0
        '';
      };

      home = {
        file = {
          ".p10k.zsh" = {
            source = ./dotfiles/dot_p10k.zsh;
            executable = true;
          };
          "${configDirectory}/oh-my-zsh-custom/themes/powerlevel10k" = {
            source = "${pkgs.zsh-powerlevel10k.outPath}/share/zsh-powerlevel10k";
            recursive = false;
          };
        };
      };

      programs = {
        zsh = {
          enable = true;
          oh-my-zsh = {
            enable = true;
            custom = "${configDirectory}/oh-my-zsh-custom";
            theme = "powerlevel10k/powerlevel10k";
            plugins = [ ];
          };
          plugins = [
            {
              name = "powerlevel10k-config";
              src = ./dotfiles;
              file = "dot_p10k.zsh";
            }
            {
              name = "zsh-autosuggestions";
              src = "${pkgs.zsh-autosuggestions.outPath}/share/zsh-autosuggestions";
              file = "zsh-autosuggestions.zsh";
            }
            {
              name = "zsh-syntax-highlighting";
              src = "${pkgs.zsh-syntax-highlighting.outPath}/share/zsh-syntax-highlighting";
              file = "zsh-syntax-highlighting.zsh";
            }
          ];
        };

        alacritty = {
          enable = true;

          settings = {
            window = {
              padding = {
                x = 0;
                y = 5;
              };
              startup_mode = "Maximized";
            };
            font.size = 11.0;
            colors = {
              primary = {
                background = "0x2E3440";
                foreground = "0xD8DEE9";
              };
              cursor = {
                text = "0x2E3440";
                cursor = "0xD8DEE9";
              };
              normal = {
                black = "0x3B4252";
                red = "0xBF616A";
                green = "0xA3BE8C";
                yellow = "0xEBCB8B";
                blue = "0x81A1C1";
                magenta = "0xB48EAD";
                cyan = "0x88C0D0";
                white = "0xE5E9F0";
              };
              bright = {
                black = "0x4C566A";
                red = "0xBF616A";
                green = "0xA3BE8C";
                yellow = "0xEBCB8B";
                blue = "0x81A1C1";
                magenta = "0xB48EAD";
                cyan = "0x8FBCBB";
                white = "0xECEFF4";
              };
            };
            cursor.style = "Underline";
          };
        };

        i3status-rust = {
          enable = true;
          bars = {
            bottom = {
              blocks = [
                {
                  block = "time";
                  format = "%FT%T";
                  timezone = "Europe/Minsk";
                  interval = 0.5;
                }
                {
                  block = "net";
                  device = "enp6s0";
                  format = "{speed_down;K*b} {speed_up;K*b}";
                  interval = 5;
                }
                {
                  block = "disk_space";
                  path = "/";
                  alias = "/";
                  format = "{icon} {free:6}";
                  info_type = "available";
                  unit = "GB";
                  interval = 10;
                  warning = 20.0;
                  alert = 10.0;
                }
                {
                  block = "memory";
                  format_mem = "{mem_used:5;M}({mem_used_percents})";
                  format_swap = "{swap_used:5;M}({swap_used_percents})";
                  display_type = "memory";
                  icons = true;
                  clickable = true;
                  interval = 2;
                }
                {
                  block = "temperature";
                  collapsed = false;
                  interval = 10;
                  format = "{average}";
                  chip = "*-isa-*";
                }
                {
                  block = "cpu";
                  format = "{utilization} {frequency}";
                  interval = 0.5;
                }
                {
                  block = "notify";
                }
              ];
              icons = "material-nf";
              theme = "nord-dark";
            };
          };
        };

        vscode = {
          enable = true;
          extensions = (with pkgs.vscode-extensions; [
            ms-azuretools.vscode-docker
            ms-vscode-remote.remote-ssh
            hashicorp.terraform
          ]) ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
            {
              name = "nord-visual-studio-code";
              publisher = "arcticicestudio";
              version = "0.19.0";
              sha256 = "6b06ea16fe98b98234b7333f41b1d14d4978076bcd51dcb9d85e273e6bfe7515";
            }
            {
              name = "intellij-idea-keybindings";
              publisher = "k--kato";
              version = "1.4.5";
              sha256 = "dd04e5fce50f94b8fd5387253802043254bdc5d488c19a207fe56262fddd7c71";
            }
            {
              name = "nix-ide";
              publisher = "jnoortheen";
              version = "0.1.16";
              sha256 = "77f5369de2d50d98c2829b06600da4be60265f25bfebb179ae214be97f0d7e12";
            }
            {
              name = "PowerShell";
              publisher = "ms-vscode";
              version = "2021.10.1";
              sha256 = "029da106d46f22dcbf5a3a27e2cd307db488c62ae422a1cc13a61dda6ba498af";
            }
          ];
        };
      };

      services = {
        picom = {
          enable = true;
          backend = "glx";
          shadow = true;
          fade = true;
          shadowOpacity = "0.35";
          extraOptions = ''
            corner-radius = 3.0;

            blur-background = true;
            blur-background-frame = true;
            blur-background-fixed = true;
            blur-kern = "3x3box";
            blur-method = "kernel";
            blur-strength = 16;

            shadow-radius = 12;
            shadow-offset-x = -7;
            shadow-offset-y = -7;

            fade-in-step = 0.056;
            fade-out-step = 0.06;
          '';
        };
      };
    };
  };

  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
    };
  };

  hardware = {
    video.hidpi.enable = lib.mkDefault true;
    pulseaudio = {
      enable = true;
      support32Bit = true;
      extraConfig = ''
      set-default-sink 1
      set-sink-mute 1 0
      set-sink-volume 1 0x3000
      '';
    };
  };

  system.stateVersion = "21.05";
}
